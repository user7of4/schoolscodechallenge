//
//  ScoresTests.swift
//  SchoolTests
//
//  Created by Ryan Osborn on 10/23/18.
//  Copyright © 2018 Ryan Osborn. All rights reserved.
//

import XCTest
@testable import Schools

class ScoresTests: XCTestCase {
    var scores:Scores?
    
    override func setUp() {
        
        let scoreDictionary = [
            "sat_critical_reading_avg_score":"395",
            "sat_math_avg_score":"401",
            "sat_writing_avg_score":"382",
            "school_name":"SCHOOL OF HARD KNOCKS"
        ]

        scores = Scores(scoreDictionary)
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testReadingScore() {
        XCTAssertEqual("395", scores?.readingAvgScore, "expect reading score 395")
        XCTAssertEqual("reading: 395", scores?.describeReadingScore(), "expect reading: 395")
    }

    func testMathScore() {
        XCTAssertEqual("401", scores?.mathAvgScore, "expect math score 401")
        XCTAssertEqual("math: 401", scores?.describeMathScore(), "expect math: 401")
    }

    func testWritingScore() {
        XCTAssertEqual("382", scores?.writingAvgScore, "expect math score 382")
        XCTAssertEqual("writing: 382", scores?.describeWritingScore(), "expect writing: 382")
    }

    func testName() {
        XCTAssertEqual("SCHOOL OF HARD KNOCKS", scores?.name, "expect name SCHOOL OF HARD KNOCKS")
    }
}
