//
//  SchoolTests.swift
//  SchoolsTests
//
//  Created by Ryan Osborn on 10/23/18.
//  Copyright © 2018 Ryan Osborn. All rights reserved.
//

import XCTest
@testable import Schools

class SchoolTests: XCTestCase {
    var school:School?
    
    override func setUp() {
        let schoolDictionary = [
            "school_name":"SCHOOL OF HARD KNOCKS"
        ]
        
        school = School(schoolDictionary)
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    func testName() {
        XCTAssertEqual("SCHOOL OF HARD KNOCKS", school?.name, "expect name SCHOOL OF HARD KNOCKS")
    }
}
