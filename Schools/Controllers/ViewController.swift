//
//  ViewController.swift
//  Schools
//
//  Created by Ryan Osborn on 10/23/18.
//  Copyright © 2018 Ryan Osborn. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, ScoresViewControllerDelegate {
    lazy var tableView: UITableView = {
        let view = UITableView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.delegate = self
        view.dataSource = self
        view.register(SchoolTableViewCell.self, forCellReuseIdentifier: SchoolTableViewCell.reuseIdentifier())
        view.rowHeight = 44.0
        return view
    }()
    
    lazy var scoresViewController: ScoresViewController = {
        let viewController = ScoresViewController()
        viewController.delegate = self
        return viewController
    }()
    
    var schoolScores:[String:Scores] = [:]
    var schools:[School] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        view.addSubview(tableView)
        
        tableView.topAnchor.constraint(equalTo:view.topAnchor).isActive = true
        tableView.leadingAnchor.constraint(equalTo:view.leadingAnchor).isActive = true
        tableView.trailingAnchor.constraint(equalTo:view.trailingAnchor).isActive = true
        tableView.bottomAnchor.constraint(equalTo:view.bottomAnchor).isActive = true
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        Api.getSATScores(
            success: { (result) in
                for scores in result {
                    let scores = Scores(scores)
                    self.schoolScores[scores.name] = scores
                }

                Api.getSchools(
                    success: { (result) in
                        for school in result {
                            let school = School(school)
                            self.schools.append(school)
                        }
                        
                        self.tableView.reloadData()
                },
                    failure: { (result) in
                        print("error: \(result)")
                })
        },
            failure: { (result) in
                print("error: \(result)")
        })
    }
    
    // pragma mark table view delegate and data source
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return schools.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier:SchoolTableViewCell.reuseIdentifier(), for: indexPath) as! SchoolTableViewCell
        cell.titleLabel.text = schools[indexPath.row].name
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let name = schools[indexPath.row].name.uppercased()
        
        if let scores = schoolScores[name] {
            scoresViewController.scores = scores
            present(scoresViewController, animated: true, completion: nil)
        }
    }
    
    func closeButtonPressed() {
        scoresViewController.dismiss(animated: true, completion: nil)
    }
}
