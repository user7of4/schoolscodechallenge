//
//  ScoresViewController.swift
//  Schools
//
//  Created by Ryan Osborn on 10/23/18.
//  Copyright © 2018 Ryan Osborn. All rights reserved.
//

import Foundation
import UIKit

protocol ScoresViewControllerDelegate: class {
    func closeButtonPressed()
}

class ScoresViewController: UIViewController {
    lazy var mathLabel: UILabel = {
        let view = UILabel()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.textAlignment = NSTextAlignment.center
        return view
    }()
    
    lazy var readingLabel: UILabel = {
        let view = UILabel()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.textAlignment = NSTextAlignment.center
        return view
    }()

    lazy var writingLabel: UILabel = {
        let view = UILabel()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.textAlignment = NSTextAlignment.center
        return view
    }()

    lazy var closeButton: UIButton = {
        let view = UIButton()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.setTitle("Close", for: .normal)
        view.setTitleColor(UIColor.black, for: .normal)
        view.addTarget(self, action: #selector(self.closeButtonPressed), for: .touchUpInside)
        view.backgroundColor = UIColor.yellow
        return view
    }()
    
    weak var delegate:ScoresViewControllerDelegate?
    var scores:Scores?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.white

        view.addSubview(mathLabel)
        view.addSubview(readingLabel)
        view.addSubview(writingLabel)
        view.addSubview(closeButton)

        mathLabel.topAnchor.constraint(equalTo:view.topAnchor, constant:200).isActive = true
        mathLabel.leadingAnchor.constraint(equalTo:view.leadingAnchor).isActive = true
        mathLabel.trailingAnchor.constraint(equalTo:view.trailingAnchor).isActive = true
        
        readingLabel.topAnchor.constraint(equalTo:mathLabel.bottomAnchor).isActive = true
        readingLabel.leadingAnchor.constraint(equalTo:view.leadingAnchor).isActive = true
        readingLabel.trailingAnchor.constraint(equalTo:view.trailingAnchor).isActive = true

        writingLabel.topAnchor.constraint(equalTo:readingLabel.bottomAnchor).isActive = true
        writingLabel.leadingAnchor.constraint(equalTo:view.leadingAnchor).isActive = true
        writingLabel.trailingAnchor.constraint(equalTo:view.trailingAnchor).isActive = true
        
        closeButton.topAnchor.constraint(equalTo:writingLabel.bottomAnchor, constant:20).isActive = true
        closeButton.centerXAnchor.constraint(equalTo:writingLabel.centerXAnchor).isActive = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
     
        if let scores = scores {
            mathLabel.text = scores.describeMathScore()
            readingLabel.text = scores.describeReadingScore()
            writingLabel.text = scores.describeWritingScore()
        }
    }
    
    @objc func closeButtonPressed() {
        delegate?.closeButtonPressed()
    }
}
