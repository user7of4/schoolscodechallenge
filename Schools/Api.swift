//
//  Api.swift
//  Schools
//
//  Created by Ryan Osborn on 10/23/18.
//  Copyright © 2018 Ryan Osborn. All rights reserved.
//

import Foundation
import AFNetworking


class Api {
    static let manager: AFHTTPSessionManager = {
        let manager:AFHTTPSessionManager = AFHTTPSessionManager()
        manager.requestSerializer = AFJSONRequestSerializer()
        manager.requestSerializer.setValue("application/json", forHTTPHeaderField:"Content-Type")
        manager.requestSerializer.setValue("application/json", forHTTPHeaderField:"Accept")
        return manager
    }()

    static func getSchools(
        success:@escaping ([[String:String]]) -> Void,
        failure:@escaping (NSError) -> Void) {
        let url = URL(string:"https://data.cityofnewyork.us/resource/97mf-9njv.json")!
        
        manager.get(url.absoluteString, parameters:nil, progress:nil, success:{
            (task: URLSessionDataTask, responseObject: Any?) in
            success(responseObject as! [[String:String]])
        }, failure: {
            (task:URLSessionDataTask?, error:Error) in
            failure(error as NSError)
        })
    }
    
    static func getSATScores(
        success:@escaping ([[String:String]]) -> Void,
        failure:@escaping (NSError) -> Void) {
        let url = URL(string:"https://data.cityofnewyork.us/resource/734v-jeq5.json")!
        
        manager.get(url.absoluteString, parameters:nil, progress:nil, success:{
            (task: URLSessionDataTask, responseObject: Any?) in
            success(responseObject as! [[String:String]])
        }, failure: {
            (task:URLSessionDataTask?, error:Error) in
            failure(error as NSError)
        })
    }
}
