//
//  SchoolTableViewCell.swift
//  Schools
//
//  Created by Ryan Osborn on 10/23/18.
//  Copyright © 2018 Ryan Osborn. All rights reserved.
//

import Foundation
import UIKit


class SchoolTableViewCell:UITableViewCell {
    static func reuseIdentifier() -> String {
        return "\(type(of: self))"
    }
    
    lazy var titleLabel:UILabel = {
        let view = UILabel()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    public override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        contentView.addSubview(titleLabel)
        
        titleLabel.topAnchor.constraint(equalTo:contentView.topAnchor).isActive = true
        titleLabel.leadingAnchor.constraint(equalTo:contentView.leadingAnchor).isActive = true
        titleLabel.trailingAnchor.constraint(equalTo:contentView.trailingAnchor).isActive = true
        titleLabel.bottomAnchor.constraint(equalTo:contentView.bottomAnchor).isActive = true
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func prepareForReuse() {
        titleLabel.text = nil
    }
}
