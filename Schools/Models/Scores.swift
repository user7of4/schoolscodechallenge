//
//  Scores.swift
//  Schools
//
//  Created by Ryan Osborn on 10/23/18.
//  Copyright © 2018 Ryan Osborn. All rights reserved.
//

import Foundation

struct Scores {
    var readingAvgScore: String
    var mathAvgScore:String
    var writingAvgScore:String
    var name:String
    
    init(_ scores:[String:String]) {
        readingAvgScore = scores["sat_critical_reading_avg_score"] ?? ""
        mathAvgScore = scores["sat_math_avg_score"] ?? ""
        writingAvgScore = scores["sat_writing_avg_score"] ?? ""
        name = scores["school_name"] ?? ""
    }
    
    func describeMathScore() -> String {
        return "math: \(mathAvgScore)"
    }
    
    func describeReadingScore() -> String {
        return "reading: \(readingAvgScore)"
    }

    func describeWritingScore() -> String {
        return "writing: \(writingAvgScore)"
    }
}
