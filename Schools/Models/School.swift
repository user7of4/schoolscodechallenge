//
//  School.swift
//  Schools
//
//  Created by Ryan Osborn on 10/23/18.
//  Copyright © 2018 Ryan Osborn. All rights reserved.
//

import Foundation


struct School {
    var name:String
    
    init(_ school:[String:String]) {
        name = school["school_name"] ?? ""
    }
}
